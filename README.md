## Mellow Shallows

_A map for Supreme Commander: Forged Alliance (Forever)_

![](/images/overview.png)

## Statistics of the map

![](/images/island-large.png)

The map is designed to be either be played as a 2 vs 2 or 3 vs 3. Both teams start on a 'corner' of the map, there are numerous islands for them to take. Each player starts with atleast 9 mass extractors in their near vicinity, on the original island. Up to 12 extractors can be found on the smaller islands for each team.

There is reclaim on this map. This includes:
 - A lot of trees for energy.
 - A few units (t1 frigates / t2 destroyer).

The islands have the frigate wreckages. The player at the tip of the island starts with a destroyer to reclaim due to its vurnerability.

![](/images/island-small.png)

The map has various pieces of code to add additional feeling to the map. This includes:
 - For the civs that are underwater: when the shield drops the structures are quickly destroyed under the pressure of water.

## Notes about the imagery

 - The units with dark green icons represent wreckages (on land) and the civilian shield holding back the water (on water).

## License

The stratum layers (/env/layers) are from www.textures.com. I am obligated to add this text:
_One or more textures bundled with this project have been created with images from Textures.com. These images may not be redistributed by default. Please visit www.textures.com for more information._

All other assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).