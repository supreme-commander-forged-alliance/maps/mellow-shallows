local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

function OnPopulate()
	
	ScenarioInfo.path = '/maps/mellow_shallows.v0008/';
	
	ScenarioUtils.InitializeArmies()
	ScenarioFramework.SetPlayableArea(ScenarioUtils.AreaToRect('PlayableArea'), false)
end

function OnStart(self)

	-- spawn the civs that are underwater, make them all vanish if the shield drops / changes
	doscript(ScenarioInfo.path .. "scripts/civs.lua")

end
