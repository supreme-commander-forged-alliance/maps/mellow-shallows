local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

function OnPopulate()
	ForkThread(
		function()
			-- initial armies, make the game start
			ScenarioUtils.InitializeArmies()

			-- shrinken the map slightly
			local area = 'PlayableArea'
			local rect = ScenarioUtils.AreaToRect(area)

			-- make the rectangle a multiple of 4
			local x0 = rect.x0 - math.mod(rect.x0 , 4)
			local y0 = rect.y0 - math.mod(rect.y0 , 4)
			local x1 = rect.x1 - math.mod(rect.x1, 4)
			local y1 = rect.y1 - math.mod(rect.y1, 4)

			-- store it where the AI can find it
			if ScenarioInfo.MapData then 
				ScenarioInfo.MapData.PlayableRect = {x0,y0,x1,y1}
			end

			-- set the actual playable area
			rect.x0 = x0
			rect.x1 = x1
			rect.y0 = y0
			rect.y1 = y1
			SetPlayableRect( x0, y0, x1, y1 )
		
			-- sync it with the UI
			import('/lua/SimSync.lua').SyncPlayableRect(rect)
		end 
	)
end

function OnStart(self)
end
